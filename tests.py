from twisted.trial.unittest import TestCase
from twisted.test.proto_helpers import StringTransport
from twisted.internet.protocol import Factory
from twisted.internet import task

import mock

from base import BaseBotProtocol, requires

class TestUserModeBookkeeping(TestCase):
    def setUp(self):
        factory = Factory()
        factory.channels = ["channel"]

        proto = BaseBotProtocol()
        proto.factory = factory
        proto.nickname = 'testbot'
        proto.transport = StringTransport()
        proto.signedOn()

        self.proto = proto

    def assert_nick_modes(self, channel, nick, modes, message=None):

        actual_modes = self.proto.names[channel][nick]
        if message is None:
            modes_message = ("{nick} should have modes {modes} on channel "
                "{channel}, instead had {actual_modes}").format(
                    nick=nick,
                    channel=channel,
                    modes=''.join(modes),
                    actual_modes=''.join(actual_modes)
                )
        else:
            modes_message = message
        self.assertItemsEqual(modes, actual_modes, modes_message)


    def test_parses_modes_from_names_on_join(self):
        self.proto.joined("channel")
        names = "foo +bar ~baz @qux @+quux"
        self.proto.irc_RPL_NAMREPLY("server", ("nick", "prefix", "channel", names))
        self.assert_nick_modes("channel", "foo", '')
        self.assert_nick_modes("channel", "bar", 'v')
        self.assert_nick_modes("channel", "baz", 'q')
        self.assert_nick_modes("channel", "qux", 'o')
        self.assert_nick_modes("channel", "quux", 'ov')

    def tests_refetch_modes_after_rejoinin_channel(self):
        # bot joins a channel
        self.proto.joined("channel")
        names = "foo +bar ~baz @qux @+quux"
        self.proto.irc_RPL_NAMREPLY("server", ("nick", "prefix", "channel", names))

        # now the bot leaves the channel
        self.proto.left("channel")
        # someone else leaves the channel, someone gets their mode changed
        names = "+foo baz @+qux @+quux"
        self.proto.joined("channel")
        self.proto.irc_RPL_NAMREPLY("server", ("nick", "prefix", "channel", names))

        self.assert_nick_modes("channel", "foo", 'v')
        self.assert_nick_modes("channel", "baz", '')
        self.assert_nick_modes("channel", "qux", 'vo')
        self.assert_nick_modes("channel", "quux", 'ov')

    def test_user_rejoins(self):
        self.proto.joined("channel")
        names = "@foo"
        self.proto.irc_RPL_NAMREPLY("server", ("nick", "prefix", "channel", names))

        self.proto.userLeft("foo", "channel")
        self.proto.userJoined("foo", "channel")
        self.assert_nick_modes("channel", "foo", '')

        self.proto.userQuit("foo", "channel")
        self.proto.userJoined("foo", "channel")
        self.assert_nick_modes("channel", "foo", '')

    def test_user_changes_nick(self):
        self.proto.joined("channel")
        names = "@foo"
        self.proto.irc_RPL_NAMREPLY("server", ("nick", "prefix", "channel", names))

        self.proto.userRenamed('foo', 'bar')
        # another user named foo joins the channel
        self.proto.userJoined('foo', 'channel')
        self.assert_nick_modes("channel", "foo", '')
        self.assert_nick_modes("channel", "bar", 'o')


class TestPermissionsChecking(TestCase):
    def setUp(self):
        factory = Factory()
        factory.channels = ["channel"]
        mock_command = mock.Mock()
        class TestingProto(BaseBotProtocol):
            # can't use mock directly since it'll error out on @wraps
            @requires("oh")
            def command_COMMAND(self, *args, **kwargs):
                return mock_command()

        proto = TestingProto()
        proto.factory = factory
        proto.nickname = 'testbot'
        proto.transport = StringTransport()
        proto.signedOn()

        self.proto = proto
        self.mock_command = mock_command

    def test_user_without_flag_cannot_run_command(self):
        self.proto.joined("channel")
        names = "+foo"
        self.proto.irc_RPL_NAMREPLY("server", ("nick", "prefix", "channel", names))
        self.proto.privmsg("foo", "channel", "!command")
        self.assertEqual(False, self.mock_command.called,
            "Command called while the caller didn't have the required flag")

    def test_user_with_flag_can_run_command(self):
        self.proto.joined("channel")
        names = "@foo"
        self.proto.irc_RPL_NAMREPLY("server", ("nick", "prefix", "channel", names))
        self.proto.privmsg("foo", "channel", "!command")
        self.assertEqual(True, self.mock_command.called,
            "Command wasn't called but the user had required flags")


class TestBufferedSay(TestCase):
    def setUp(self):
        mock_say_pastebin = mock.Mock()
        mock_say = mock.Mock()

        class TestingProto(BaseBotProtocol):
            def say_pastebin(self, *args, **kwargs):
                mock_say_pastebin(*args, **kwargs)
                super(TestingProto, self).say_pastebin(*args, **kwargs)
            def say(self, channel, line):
                mock_say(channel, line)

        proto = TestingProto()

        factory = Factory()
        factory.channels = ["channel"]
        proto.factory = factory
        clock = task.Clock()
        proto.callLater = clock.callLater
        proto.nickname = 'testbot'
        proto.transport = StringTransport()
        proto.signedOn()

        self.proto = proto
        self.mock_say_pastebin = mock_say_pastebin
        self.clock = clock
        self.mock_say = mock_say

    def test_doesnt_send_immediately_when_using_buffered_say(self):
        self.proto.joined("chan")
        self.proto.say_buffered("chan", "fooo")
        self.assertEqual(False, self.mock_say_pastebin.called,
            "Lines sent immediately instead of waiting for more input with say_buffered")

    @mock.patch('base.getPage')
    def test_send_from_buffer_after_time_has_passed(self, mock_getPage):
        mock_getPage.return_value = '{"data": "fake_mock_url"}'
        self.proto.joined("chan")
        self.proto.say_buffered("chan", "fooo")
        self.clock.advance(1)
        self.assertEqual(True, self.mock_say_pastebin.called,
            "Should have sent the lines from say buffer")
        self.assertEqual(True, self.mock_say.called,
            "Should have sent the url to the paste")