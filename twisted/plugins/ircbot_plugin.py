from zope.interface import implements

from twisted.python import usage
from twisted.plugin import IPlugin
from twisted.application.service import IServiceMaker
from twisted.application import internet

from base import BaseBotFactory


class Options(usage.Options):
    optParameters = [
        ['HOST', 'h', 'irc.rizon.net', 'IRC server to connect'],
        ['PORT', 'p', 6667, 'Port to connect to the irc server on', int],
        ['channel', 'c', '#basebottest', 'Channel'],
        ['nickname', 'n', 'asdf', 'Nick'],
        ['password', 'w', None, 'Server password'],
        ['dbname', 'd', 'database.sqlite3', 'Database to use'],
    ]


class BotServiceMaker(object):
    implements(IServiceMaker, IPlugin)
    tapname = "ircbot"
    description = "ircbot"
    options = Options

    def makeService(self, options):
        return internet.TCPClient(options['HOST'], options['PORT'],
            BaseBotFactory(**options))


serviceMaker = BotServiceMaker()
