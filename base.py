#coding: utf-8
from collections import defaultdict
from functools import wraps
from itertools import izip_longest
import json
import random
import re

from twisted.internet.defer import inlineCallbacks, Deferred
from twisted.internet import reactor, task, protocol
from twisted.words.protocols import irc
from twisted.enterprise import adbapi
from twisted.web.client import getPage

def requires(flags):
    def decorator(f):
        @wraps(f)
        def inner(self, channel, nickname, words):
            user_mode = self.names[channel][nickname]
            if any(flag in user_mode for flag in flags):
                return f(self, channel, nickname, words)
        return inner
    return decorator

class BaseBotProtocol(irc.IRCClient, object):
    callLater = reactor.callLater
    nickname = ''
    _say_buffered_timeout = None

    def signedOn(self):
        self._say_buffer = defaultdict(list)
        
        self.names = defaultdict(lambda: defaultdict(set))
        self.topics = {}
        self.channels = set()
        for chan in self.factory.channels:
            if isinstance(chan, unicode):
                chan = chan.encode('utf-8')
            self.join(chan)

    def joined(self, channel):
        self.channels.add(channel)

    def left(self, channel):
        self.channels.remove(channel)
        del self.names[channel]

    def privmsg(self, user, channel, message):
        nickname, sep, rest = user.partition('!')

        if not message.startswith('!'):
            return

        words = message.split()
        command = words[0].lstrip('!').upper()
        handler = getattr(self, 'command_%s' % command, None)

        if handler is not None:
            handler(channel, nickname, words[1:])

    def irc_RPL_NAMREPLY(self, server, (nick, prefix, channel, names)):
        store = self.names[channel]
        for name in names.split():
            mode, nick = self.splitMode(name)
            store[nick] = set(mode)

    def irc_INVITE(self, inviter, (nick, channel)):
        self.join(channel)

    def splitMode(self, name):
        modes = []
        while name.startswith(('+' ,'@', '~', '%')):
            mode, name = name[0], name[1:]
            _mode_translation = {
                '+': 'v',
                '@': 'o',
                '~': 'q',
                '%': 'h'
            }
            modes.append(_mode_translation.get(mode, ''))
        return ''.join(modes), name

    def topicUpdated(self, user, channel, newTopic):
        self.topics[channel] = newTopic

    def modeChanged(self, user, channel, set, modes, args):
        if not channel in self.channels:
            return

        for mode, nick in zip(modes, args):
            if set:
                self.names[channel][nick].add(mode)
            else:
                self.names[channel][nick].remove(mode)

    def userLeft(self, nick, channel):
        del self.names[channel][nick]

    def userQuit(self, nick, channel):
        del self.names[channel][nick]

    def userRenamed(self, oldname, newname):
        for channel, modes in self.names.iteritems():
            self.names[channel][newname] = self.names[channel].pop(oldname)

    def wait(self, time):
        dfd = Deferred()
        self.callLater(time, dfd.callback, None)
        return dfd

    def say(self, channel, message, length=None):
        if isinstance(message, unicode):
            # we don't want to use .encode all over the place, let's just encode
            # unicode objects just prior to sending them over the wire. Hardcode utf-8
            # as irc doesn't have a way of encoding negotiation anyway
            message = message.encode('utf-8')
        return super(BaseBotProtocol, self).say(channel, message, length)

    def say_buffered(self, channel, message):
        self._say_buffer[channel].append(message)
        if self._say_buffered_timeout is not None:
            self._say_buffered_timeout.cancel()
        self._say_buffered_timeout = self.callLater(1, self._send_buffer, channel)

    @inlineCallbacks
    def _send_buffer(self, channel):
        yield self.say_pastebin(channel, self._say_buffer[channel])
        self._say_buffer[channel] = []

    @inlineCallbacks
    def say_pastebin(self, channel, lines, message=None):
        lines = '\n'.join(lines)
        pasted = yield getPage('http://bpaste.net/json/?method=pastes.newPaste', method='POST',
            postdata=json.dumps({
                'language': 'text', 
                'code': lines, 
                'private': True
        }))
        paste_id = json.loads(pasted)['data']
        url = u'http://bpaste.net/show/' + paste_id
        if message is not None:
            message = message % (url, )
        else:
            message = url
        self.say(channel, message)


class BaseBotFactory(protocol.ReconnectingClientFactory):
    protocol = BaseBotProtocol
    def __init__(self, **kwargs):
        for k, v in kwargs.iteritems():
            setattr(self, k, v)
        self.dbpool = adbapi.ConnectionPool('sqlite3', self.dbname)

    def buildProtocol(self, addr):
        proto = protocol.ReconnectingClientFactory.buildProtocol(self, addr)
        proto.nickname = self.nickname
        proto.password = self.password
        return proto
